/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int barborder     = 2;        /* border pixel of bar */
static const unsigned int borderpx      = 2;        /* border pixel of windows */
static const unsigned int snap          = 32;       /* snap pixel */
static const unsigned int gappih        = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv        = 10;       /* vert inner gap between windows */
static const unsigned int gappoh        = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov        = 10;       /* vert outer gap between windows and screen edge */
static const int smartgaps_fact         = 1;        /* gap factor when there is only one client; 0 = no gaps, 3 = 3x outer gaps */
static const int swallowfloating        = 0;        /* 1 means swallow floating windows by default */
static const int showbar                = 1;        /* 0 means no bar */
static const int showtitle              = 1;        /* 0 means no title */
static const int topbar                 = 1;        /* 0 means bottom bar */
static const int bargaps                = 1;        /* 0 means no vertpad or sidepad */
static       int vertpad                = 10;       /* vertical padding of bar */
static       int sidepad                = 10;       /* horizontal padding of bar */
static const int horizpadbar            = 2;        /* horizontal padding for statusbar */
static const int vertpadbar             = 8;        /* vertical padding for statusbar */
static const int attachmode             = 3;        /* 0 master (default), 1 = above, 2 = aside, 3 = below, 4 = bottom */
static const int focusedontop           = 0;        /* 1 means focused client is shown on top of floating windows */
static int floatposgrid_x               = 5;        /* float grid columns */
static int floatposgrid_y               = 5;        /* float grid rows */
static const char *fonts[]              = { "monospace:size=10" };
static const char dmenufont[]           = "monospace:size=10";
static char c000000[]                   = "#000000";        /* placeholder value */
static char normfgcolor[]               = "#e2d3ba";
static char normbgcolor[]               = "#1d2021";
static char normbordercolor[]           = "#1d2021";
static char selfgcolor[]                = "#1d2021";
static char selbgcolor[]                = "#e2d3ba";
static char selbordercolor[]            = "#e2d3ba";
static char scratchnormfgcolor[]        = "#e2d3ba";
static char scratchnormbgcolor[]        = "#1d2021";
static char scratchnormbordercolor[]    = "#ea6962";
static char scratchselfgcolor[]         = "#e2d3ba";
static char scratchselbgcolor[]         = "#1d2021";
static char scratchselbordercolor[]     = "#a9b665";
static char titlenormfgcolor[]          = "#e2d3ba";
static char titlenormbgcolor[]          = "#1d2021";
static char titlenormbordercolor[]      = "#1d2021";
static char titleselfgcolor[]           = "#1d2021";
static char titleselbgcolor[]           = "#e2d3ba";
static char titleselbordercolor[]       = "#1d2021";
static char urgfgcolor[]                = "#e2d3ba";
static char urgbgcolor[]                = "#1d2021";
static char urgbordercolor[]            = "#ff0000";
static char tagsfgcolor[]               = "#e2d3ba";
static char tagsbgcolor[]               = "#1d2021";
static char tag1fgcolor[]               = "#89b482";
static char tag1bgcolor[]               = "#1d2021";
static char tag2fgcolor[]               = "#d8a657";
static char tag2bgcolor[]               = "#1d2021";
static char tag3fgcolor[]               = "#ef938e";
static char tag3bgcolor[]               = "#1d2021";
static char tag4fgcolor[]               = "#d4be98";
static char tag4bgcolor[]               = "#1d2021";
static char tag5fgcolor[]               = "#d3869b";
static char tag5bgcolor[]               = "#1d2021";
static char tag6fgcolor[]               = "#7daea3";
static char tag6bgcolor[]               = "#1d2021";
static char tag7fgcolor[]               = "#ea6962";
static char tag7bgcolor[]               = "#1d2021";
static char tag8fgcolor[]               = "#d3869b";
static char tag8bgcolor[]               = "#1d2021";
static char tag9fgcolor[]               = "#e1bb7e";
static char tag9bgcolor[]               = "#1d2021";
static char layoutfgcolor[]             = "#ae81ff";
static char layoutbgcolor[]             = "#1d2021";
static char termcol0[]                  = "#000000"; /* black   */
static char termcol1[]                  = "#ff0000"; /* red     */
static char termcol2[]                  = "#33ff00"; /* green   */
static char termcol3[]                  = "#ff0099"; /* yellow  */
static char termcol4[]                  = "#0066ff"; /* blue    */
static char termcol5[]                  = "#cc00ff"; /* magenta */
static char termcol6[]                  = "#00ffff"; /* cyan    */
static char termcol7[]                  = "#d0d0d0"; /* white   */
static char termcol8[]                  = "#808080"; /* black   */
static char termcol9[]                  = "#ff0000"; /* red     */
static char termcol10[]                 = "#33ff00"; /* green   */
static char termcol11[]                 = "#ff0099"; /* yellow  */
static char termcol12[]                 = "#0066ff"; /* blue    */
static char termcol13[]                 = "#cc00ff"; /* magenta */
static char termcol14[]                 = "#00ffff"; /* cyan    */
static char termcol15[]                 = "#ffffff"; /* white   */
static char *termcolor[] = {
  termcol0, termcol1, termcol2, termcol3, termcol4, termcol5,
  termcol6, termcol7, termcol8, termcol9, termcol10,
  termcol11, termcol12, termcol13, termcol14, termcol15,
};
static char *colors[][ColCount]         = {
       /*                       fg           bg           border          */
       [SchemeNorm]         = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]          = { selfgcolor,  selbgcolor,  selbordercolor },
       [SchemeScratchNorm]  = { scratchnormfgcolor,  scratchnormbgcolor,  scratchnormbordercolor },
       [SchemeScratchSel]   = { scratchselfgcolor,  scratchselbgcolor,  scratchselbordercolor },
       [SchemeUrg]          = { urgfgcolor,  urgbgcolor,  urgbordercolor },
       [SchemeTitleNorm]    = { titlenormfgcolor,  titlenormbgcolor,  titlenormbordercolor },
       [SchemeTitleSel]     = { titleselfgcolor,  titleselbgcolor,  titleselbordercolor },
       [SchemeTags]         = { tagsfgcolor,  tagsbgcolor,  c000000 },
       [SchemeTag1]         = { tag1fgcolor,  tag1bgcolor,  c000000 },
       [SchemeTag2]         = { tag2fgcolor,  tag2bgcolor,  c000000 },
       [SchemeTag3]         = { tag3fgcolor,  tag3bgcolor,  c000000 },
       [SchemeTag4]         = { tag4fgcolor,  tag4bgcolor,  c000000 },
       [SchemeTag5]         = { tag5fgcolor,  tag5bgcolor,  c000000 },
       [SchemeTag6]         = { tag6fgcolor,  tag6bgcolor,  c000000 },
       [SchemeTag7]         = { tag7fgcolor,  tag7bgcolor,  c000000 },
       [SchemeTag8]         = { tag8fgcolor,  tag8bgcolor,  c000000 },
       [SchemeTag9]         = { tag9fgcolor,  tag9bgcolor,  c000000 },
       [SchemeLayout]       = { layoutfgcolor,  layoutbgcolor,  c000000 },
};

static const unsigned int baralpha = 0xd0;
static const unsigned int borderalpha = OPAQUE;
static const unsigned int alphas[][3]      = {
	/*                        fg      bg        border     */
	[SchemeNorm]          = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]           = { OPAQUE, baralpha, borderalpha },
	[SchemeScratchNorm]   = { OPAQUE, baralpha, borderalpha },
	[SchemeScratchSel]    = { OPAQUE, baralpha, borderalpha },
	[SchemeUrg]           = { OPAQUE, baralpha, borderalpha },
	[SchemeTitleNorm]     = { OPAQUE, baralpha, borderalpha },
	[SchemeTitleSel]      = { OPAQUE, baralpha, borderalpha },
	[SchemeTags]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag1]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag2]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag3]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag4]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag5]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag6]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag7]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag8]          = { OPAQUE, baralpha, borderalpha },
	[SchemeTag9]          = { OPAQUE, baralpha, borderalpha },
	[SchemeLayout]        = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static char *tagicons[][NUMTAGS] = {
	[IconsDefault]        = { "", "", "", "", "", "", "", "", "" },
	[IconsVacant]         = { "", "", "", "", "漣", "", "", "", "" },
	[IconsOccupied]       = { "", "", "", "", "漣", "", "磊", "", "" },
	[IconsSelected]       = { NULL },
};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 *	WM_WINDOW_ROLE(STRING) = role
	 *	_NET_WM_WINDOW_TYPE(ATOM) = wintype
	 */
	RULE(.class = "Firefox", .tags = 1 << 8, .switchtag = 3)
	RULE(.class = "st-256color", .isterminal = 1, .noswallow = 0)
	RULE(.class = "mpv", .noswallow = 0)
	RULE(.class = "Gcr-prompter", .isfloating = 1)
	RULE(.class = "fzfmenu", .isfloating = 1)
	RULE(.class = "Nsxiv", .tags = 1 << 3, .isfloating = 1, .switchtag = 3, .noswallow = 0)
	RULE(.instance = "spterm", .scratchkey = 's', .floatpos = "50% 50% 60% 60%", .alwaysontop = 1)
	RULE(.wintype = WTYPE "DIALOG", .isfloating = 1, .alwaysontop = 1)
	RULE(.wintype = WTYPE "UTILITY", .isfloating = 1, .alwaysontop = 1)
	RULE(.wintype = WTYPE "TOOLBAR", .isfloating = 1, .alwaysontop = 1)
	RULE(.wintype = WTYPE "SPLASH", .isfloating = 1, .alwaysontop = 1)
	RULE(.role = "pop-up", .isfloating = 1, .alwaysontop = 1)
	RULE(.role = "GtkFileChooserDialog", .isfloating = 1, .alwaysontop = 1)
	RULE(.role = "PictureInPicture", .floatpos = "50% 50% 50% 50%", .alwaysontop = 1)
	RULE(.title = "Event Tester", .noswallow = 1)
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */
static const int decorhints  = 1;    /* 1 means respect decoration hints */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "D[]",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "HHH",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#define Alt Mod1Mask
#define AltGr Mod3Mask
#define Ctrl ControlMask
#define Shift ShiftMask
#define ShiftGr Mod5Mask
#define Super Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ Super,            KEY,      comboview,      {.ui = 1 << TAG} }, \
	{ Super|Ctrl,       KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ Super|Shift,      KEY,      combotag,       {.ui = 1 << TAG} }, \
	{ Super|Ctrl|Shift, KEY,      toggletag,      {.ui = 1 << TAG} }, \
	{ Super|Alt|Shift,  KEY,      swaptags,       {.ui = 1 << TAG} }, \
	{ Super|Alt,        KEY,      tagnextmon,     {.ui = 1 << TAG} }, \
	{ Super|Alt|Ctrl,   KEY,      tagprevmon,     {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbgcolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *scratchpadcmd[] = {"s", "st", "-n", "spterm", NULL};

static const Key keys[] = {
	/* modifier                key            function            argument */
	{ Super,                   XK_p,          spawn,              {.v = dmenucmd } },
	{ Super|Shift,             XK_Return,     spawn,              {.v = termcmd } },
	{ Super,                   XK_n,          togglescratch,      {.v = scratchpadcmd } },
	{ Super|Shift,             XK_n,          removescratch,      {.v = scratchpadcmd } },
	{ Super|Ctrl,              XK_n,          setscratch,         {.v = scratchpadcmd } },
	{ Super,                   XK_b,          togglebar,          {0} },
	{ Super|Shift,             XK_b,          togglebargaps,      {0} },
	{ Ctrl,                    XK_t,          toggletitle,        {0} },
	{ Super,                   XK_j,          focusstack,         {.i = +1 } },
	{ Super,                   XK_k,          focusstack,         {.i = -1 } },
	{ Super,                   XK_i,          incnmaster,         {.i = +1 } },
	{ Super,                   XK_d,          incnmaster,         {.i = -1 } },
	{ Super,                   XK_h,          setmfact,           {.f = -0.05} },
	{ Super,                   XK_l,          setmfact,           {.f = +0.05} },
	{ Super|Shift,             XK_h,          setcfact,           {.f = +0.25} },
	{ Super|Shift,             XK_l,          setcfact,           {.f = -0.25} },
	{ Super|Shift,             XK_o,          setcfact,           {.f =  0.00} },
	{ Super,                   XK_Return,     zoom,               {0} },
	{ Super,                   XK_equal,      incrgaps,           {.i = +1 } },
	{ Super,                   XK_minus,      incrgaps,           {.i = -1 } },
	{ Super|Shift,             XK_equal,      togglegaps,         {0} },
	{ Super|Shift,             XK_minus,      defaultgaps,        {0} },
	{ Super,                   XK_Tab,        view,               {0} },
	{ Super|Shift,             XK_c,          killclient,         {0} },
	{ Super,                   XK_t,          setlayout,          {.v = &layouts[0]} },
	{ Super,                   XK_m,          setlayout,          {.v = &layouts[1]} },
	{ Super|Shift,             XK_t,          setlayout,          {.v = &layouts[5]} },
	{ Super,                   XK_g,          setlayout,          {.v = &layouts[7]} },
	{ Super|Shift,             XK_g,          setlayout,          {.v = &layouts[11]} },
	{ Super,                   XK_e,          setlayout,          {.v = &layouts[13]} },
	{ Super,                   XK_space,      setlayout,          {0} },
	{ Super|Shift,             XK_space,      togglefloating,     {0} },
	{ Super,                   XK_f,          togglefullscreen,   {0} },
	{ Super|Shift,             XK_f,          togglefakefullscreen, {0} },
	{ Super,                   XK_s,          togglesticky,       {0} },
	{ Super,                   XK_x,          togglefocusedontop, {0} },
	{ Ctrl,                    XK_Return,     transfer,           {0} },
	{ Ctrl|Shift,              XK_Return,     transferall,        {0} },
	{ Super,                   XK_0,          view,               {.ui = ~0 } },
	{ Super|Shift,             XK_0,          tag,                {.ui = ~0 } },
	{ Super,                   XK_comma,      focusmon,           {.i = -1 } },
	{ Super,                   XK_period,     focusmon,           {.i = +1 } },
	{ Super|Shift,             XK_comma,      tagmon,             {.i = -1 } },
	{ Super|Shift,             XK_period,     tagmon,             {.i = +1 } },
	{ Super|Ctrl,              XK_comma,      cyclelayout,        {.i = -1 } },
	{ Super|Ctrl,              XK_period,     cyclelayout,        {.i = +1 } },
	{ Super,                   XK_Left,       shiftview,          {.i = -1 } },
	{ Super,                   XK_Right,      shiftview,          {.i = +1 } },
	{ Super|Ctrl,              XK_Left,       shiftviewclients,   {.i = -1 } },
	{ Super|Ctrl,              XK_Right,      shiftviewclients,   {.i = +1 } },
	{ Super|Shift,             XK_Left,       shiftboth,          {.i = -1 } },
	{ Super|Shift,             XK_Right,      shiftboth,          {.i = +1 } },
	{ Super|Alt,               XK_Left,       shiftswaptags,      {.i = -1 } },
	{ Super|Alt,               XK_Right,      shiftswaptags,      {.i = +1 } },
	{ Super|Shift,             XK_j,          pushdown,           {0} },
	{ Super|Shift,             XK_k,          pushup,             {0} },
	/* Client position is limited to monitor window area */
	{ Super|Ctrl,              XK_k,          floatpos,           {.v = "  0x -26y" } }, // ↑
	{ Super|Ctrl,              XK_h,          floatpos,           {.v = "-26x   0y" } }, // ←
	{ Super|Ctrl,              XK_l,          floatpos,           {.v = " 26x   0y" } }, // →
	{ Super|Ctrl,              XK_j,          floatpos,           {.v = "  0x  26y" } }, // ↓
	/* Corner and center positioning of client */
	{ Super|Alt,               XK_k,          floatpos,           {.v = " 50%   0%" } }, // ↑
	{ Super|Alt,               XK_h,          floatpos,           {.v = "  0%  50%" } }, // ←
	{ Super,                   XK_c,          floatpos,           {.v = " 50%  50%" } }, // ·
	{ Super|Alt,               XK_l,          floatpos,           {.v = "100%  50%" } }, // →
	{ Super|Alt,               XK_j,          floatpos,           {.v = " 50% 100%" } }, // ↓
	{ Super,                   XK_F5,         xrdb,               {.v = NULL } },
	TAGKEYS(                   XK_1,                              0)
	TAGKEYS(                   XK_2,                              1)
	TAGKEYS(                   XK_3,                              2)
	TAGKEYS(                   XK_4,                              3)
	TAGKEYS(                   XK_5,                              4)
	TAGKEYS(                   XK_6,                              5)
	TAGKEYS(                   XK_7,                              6)
	TAGKEYS(                   XK_8,                              7)
	TAGKEYS(                   XK_9,                              8)
	{ Super|Shift,             XK_q,          quit,               {0} },
	{ Super,                   XK_o,          winview,            {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         Super,          Button1,        movemouse,      {0} },
	{ ClkClientWin,         Super,          Button2,        togglefloating, {0} },
	{ ClkClientWin,         Super,          Button3,        resizeorfacts,  {0} },
	{ ClkClientWin,         Super|Shift,    Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            Super,          Button1,        tag,            {0} },
	{ ClkTagBar,            Super,          Button3,        toggletag,      {0} },
	{ ClkTagBar,            0,              Button4,        cycleiconset,   {.i = +1 } },
	{ ClkTagBar,            0,              Button5,        cycleiconset,   {.i = -1 } },
};

